#!/bin/bash

## Update all assets
### Filled
wget https://gitlab.com/SUSE-UIUX/eos-icons/-/raw/master/dist/css/_eos-icons.scss -q -O ./vendor/assets/stylesheets/eos-icons.scss
wget https://gitlab.com/SUSE-UIUX/eos-icons/-/raw/master/dist/css/eos-icons.less -q -O ./vendor/assets/stylesheets/eos-icons.less
wget https://gitlab.com/SUSE-UIUX/eos-icons/-/raw/master/dist/fonts/eos-icons.eot -q -O ./vendor/assets/fonts/eos-icons.eot
wget https://gitlab.com/SUSE-UIUX/eos-icons/-/raw/master/dist/fonts/eos-icons.svg -q -O ./vendor/assets/fonts/eos-icons.svg
wget https://gitlab.com/SUSE-UIUX/eos-icons/-/raw/master/dist/fonts/eos-icons.ttf -q -O ./vendor/assets/fonts/eos-icons.ttf
wget https://gitlab.com/SUSE-UIUX/eos-icons/-/raw/master/dist/fonts/eos-icons.woff -q -O ./vendor/assets/fonts/eos-icons.woff
wget https://gitlab.com/SUSE-UIUX/eos-icons/-/raw/master/dist/fonts/eos-icons.woff2 -q -O ./vendor/assets/fonts/eos-icons.woff2
### Outlined
wget https://gitlab.com/SUSE-UIUX/eos-icons/-/raw/master/dist/css/_eos-icons-outlined.scss -q -O ./vendor/assets/stylesheets/eos-icons-outlined.scss
wget https://gitlab.com/SUSE-UIUX/eos-icons/-/raw/master/dist/css/eos-icons-outlined.less -q -O ./vendor/assets/stylesheets/eos-icons-outlined.less
wget https://gitlab.com/SUSE-UIUX/eos-icons/-/raw/master/dist/fonts/eos-icons-outlined.eot -q -O ./vendor/assets/fonts/eos-icons-outlined.eot
wget https://gitlab.com/SUSE-UIUX/eos-icons/-/raw/master/dist/fonts/eos-icons-outlined.svg -q -O ./vendor/assets/fonts/eos-icons-outlined.svg
wget https://gitlab.com/SUSE-UIUX/eos-icons/-/raw/master/dist/fonts/eos-icons-outlined.ttf -q -O ./vendor/assets/fonts/eos-icons-outlined.ttf
wget https://gitlab.com/SUSE-UIUX/eos-icons/-/raw/master/dist/fonts/eos-icons-outlined.woff -q -O ./vendor/assets/fonts/eos-icons-outlined.woff
wget https://gitlab.com/SUSE-UIUX/eos-icons/-/raw/master/dist/fonts/eos-icons-outlined.woff2 -q -O ./vendor/assets/fonts/eos-icons-outlined.woff2

## Replace url for asset-url in the scss file
find ./vendor/assets/stylesheets/eos-icons.scss -type f -exec sed -i 's/url(/asset-url(/g' {} \;
find ./vendor/assets/stylesheets/eos-icons-outlined.scss -type f -exec sed -i 's/url(/asset-url(/g' {} \;

## Remove ../fonts/ from the asset-url
### on Mac add '' right after -i. eg sed -i '' 's|foo|bar|g' ./file.scss
sed -i 's|../fonts/||g' ./vendor/assets/stylesheets/*

## Remove the hash from the asset-url
sed -E -i 's/\?([^\"]+)//g' ./vendor/assets/stylesheets/*

## Stage vendor files for the commit
git add ./vendor

## Get the package
wget https://gitlab.com/SUSE-UIUX/eos-icons/-/raw/master/package.json -q -O ./package.json

## And detect the new version of the NPM
NEWVERSION=$(grep -o '"version": "[^"]*' ./package.json | grep -o '[^"]*$')

## Delete the current gemspec
rm eos-icons-font.gemspec
## And create a new gemspec from the template
cp ./eos-icons-font.template.gemspec ./eos-icons-font.gemspec

## And add the same version number as that of the package.json (the npm)
sed -i 's/{{NEW_VERSION_GEM}}/'"$NEWVERSION"'/g' eos-icons-font.gemspec
git add ./eos-icons-font.gemspec

## Make a commit with the version number
git commit -m "Update to version '$NEWVERSION'"
git push

### Create a version tag in Git
git tag "v'$NEWVERSION"
git push --tags
