Gem::Specification.new do |spec|
  spec.name          = "eos-icons-font"
  spec.version       = "5.4.0"

  spec.authors       = ["EOS Design System team"]
  spec.email         = ["eosdesignsystem@gmail.com"]

  spec.summary       = 'The EOS iconic font for the Rails asset pipeline.'
  spec.description   = <<-EOF
    This gem allows for easy inclusion of the EOS iconic font into the Rails asset pipeline.
  EOF

  spec.homepage      = "https://gitlab.com/SUSE-UIUX/eos-icons-rails"
  spec.license       = "MIT"

  spec.metadata = {
    "bug_tracker_uri"   => "https://gitlab.com/SUSE-UIUX/eos-icons-rails/-/issues",
    "documentation_uri" => "https://eos-icons.com/docs",
    "homepage_uri"      => "https://eos-icons.com",
    "source_code_uri"   => "https://gitlab.com/SUSE-UIUX/eos-icons-rails",
    "wiki_uri"          => "https://gitlab.com/SUSE-UIUX/eos-icons/-/wikis/home"
  }

  spec.files         = Dir["{lib,vendor}/**/*"]

  spec.add_dependency "railties", ">= 3.1"
  spec.add_development_dependency "bundler", "~> 1.14"
  spec.add_development_dependency "rake", "~> 10.0"
end
